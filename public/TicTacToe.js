/* should implement dark theming */
/* should implement focusing and keyboard input */
class TicTacToe extends HTMLElement {
 static #default_pixel_size = 330;
 static #default_size = TicTacToe.#default_pixel_size + "px" // corresponds to tile-width 100px, bars-width 30px
 static #default_bar_color = "indianred";
 static #extraneous_color = "darkgrey";
 static #default_marks = [TicTacToe.getSquareHole(), TicTacToe.getCircleHole()];
 static #default_styling = "" +
/* begin css */
`
:host {
 display: grid;
 width: ${TicTacToe.#default_size};
 height: ${TicTacToe.#default_size};
 grid-template-rows: 1fr .15fr 1fr .15fr 1fr;
 grid-template-columns: 1fr .15fr 1fr .15fr 1fr;
}

:host > .bars {
 display: contents;
}
:host > .bars > .bar {
 width: 100%;
 height: 100%;
 background-color: ${TicTacToe.#default_bar_color};
}
:host > .bars > .bar:nth-child(1) {
 grid-row: 2 / 3;
 grid-column: 1 / -1;
}
:host > .bars > .bar:nth-child(2) {
 grid-row: 4 / 5;
 grid-column: 1 / -1;
}
:host > .bars > .bar:nth-child(3) {
 grid-row: 1 / -1;
 grid-column: 2 / 3;
}
:host > .bars > .bar:nth-child(4) {
 grid-row: 1 / -1;
 grid-column: 4 / 5;
}

:host > .tiles {
 display: contents;
}
:host > .tiles > .tile {
 width: 100%;
 height: 100%;
}
:host > .tiles > .tile > .mark {
 width: 100%;
 height: 100%;
}
:host > .tiles > .tile:nth-child(1) {
 grid-row: 1 / 2;
 grid-column: 1 / 2;
}
:host > .tiles > .tile:nth-child(2) {
 grid-row: 1 / 2;
 grid-column: 3 / 4;
}
:host > .tiles > .tile:nth-child(3) {
 grid-row: 1 / 2;
 grid-column: 5 / 6;
}
:host > .tiles > .tile:nth-child(4) {
 grid-row: 3 / 4;
 grid-column: 1 / 2;
}
:host > .tiles > .tile:nth-child(5) {
 grid-row: 3 / 4;
 grid-column: 3 / 4;
}
:host > .tiles > .tile:nth-child(6) {
 grid-row: 3 / 4;
 grid-column: 5 / 6;
}
:host > .tiles > .tile:nth-child(7) {
 grid-row: 5 / 6;
 grid-column: 1 / 2;
}
:host > .tiles > .tile:nth-child(8) {
 grid-row: 5 / 6;
 grid-column: 3 / 4;
}
:host > .tiles > .tile:nth-child(9) {
 grid-row: 5 / 6;
 grid-column: 5 / 6;
}
:host > .tiles > .tile.p0 {
 fill: cadetblue;
}
:host > .tiles > .tile.p1 {
 fill: burlywood;
}
:host > .tiles > .tile.extraneous {
 fill: darkgrey;
}
`
/* end css */

 static get observedAttributes() {return ["disabled", "hidden"]}

 static getCircleHole() {
  const mark = document.createElementNS("http://www.w3.org/2000/svg", "g");
  const mask = document.createElementNS("http://www.w3.org/2000/svg", "mask");
  mask.id = "circle_mask";
  mark.appendChild(mask);
  const window = document.createElementNS("http://www.w3.org/2000/svg", "rect");
  window.setAttribute("width", 100);
  window.setAttribute("height", 100);
  window.setAttribute("x", 0);
  window.setAttribute("y", 0);
  window.setAttribute("fill", "white");
  mask.appendChild(window);
  const wall = document.createElementNS("http://www.w3.org/2000/svg", "circle");
  wall.setAttribute("r", 28);
  wall.setAttribute("cx", 50);
  wall.setAttribute("cy", 50);
  wall.setAttribute("fill", "black");
  mask.appendChild(wall);
  const circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
  circle.setAttribute("r", 38);
  circle.setAttribute("cx", 50);
  circle.setAttribute("cy", 50);
  circle.setAttribute("mask", "url(#circle_mask)");
  mark.appendChild(circle);
  return mark;
 }

 static getSquareHole() {
  const mark = document.createElementNS("http://www.w3.org/2000/svg", "g");
  const mask = document.createElementNS("http://www.w3.org/2000/svg", "mask");
  mask.id = "rect_mask";
  mark.appendChild(mask);
  const window = document.createElementNS("http://www.w3.org/2000/svg", "rect");
  window.setAttribute("width", 100);
  window.setAttribute("height", 100);
  window.setAttribute("x", 0);
  window.setAttribute("y", 0);
  window.setAttribute("fill", "white");
  mask.appendChild(window);
  const wall = document.createElementNS("http://www.w3.org/2000/svg", "rect");
  wall.setAttribute("width", 50);
  wall.setAttribute("height", 50);
  wall.setAttribute("x", 25);
  wall.setAttribute("y", 25);
  wall.setAttribute("rx", 2);
  wall.setAttribute("fill", "black");
  mask.appendChild(wall);
  const rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
  rect.setAttribute("width", 70);
  rect.setAttribute("height", 70);
  rect.setAttribute("x", 15);
  rect.setAttribute("y", 15);
  rect.setAttribute("rx", 3);
  rect.setAttribute("mask", "url(#rect_mask)");
  mark.appendChild(rect);
  return mark;
 }

 #marks
 #player
 #board
 #hidden_style
 #disabled_style
 #click_handler
 constructor() {
  super();
  this.attachShadow({mode: 'open'});
  this.#setStylesheets(this.shadowRoot);
  this.#player = 0;
  this.#click_handler = this.#getClickHandler();
  this.addEventListener("click", this.#click_handler);
  this.#board = [
   null, null, null,
   null, null, null,
   null, null, null
  ];
  this.#marks = [TicTacToe.#default_marks[0], TicTacToe.#default_marks[1]];
 }

 connectedCallback() {
  this.shadowRoot.appendChild(this.#getBars());
  this.shadowRoot.appendChild(this.#getTiles());
  TicTacToe.observedAttributes.forEach(attribute => {
   this.#attributeChangedCallback(attribute, undefined, this[attribute]);
  });
 }

 attributeChangedCallback(name, old, cur) {
  if (this?.shadowRoot.styleSheets.length == 3) {
   this.#attributeChangedCallback(name, old, cur);
  }
 }

 get disabled() {
  return this.hasAttribute("disabled");
 }
 set disabled(yes) {
  if (yes) this.setAttribute('disabled', '');
  else this.removeAttribute('disabled');
 }
 get hidden() {
  return this.hasAttribute("hidden");
 }
 set hidden(yes) {
  if (yes) this.setAttribute('hidden', '');
  else this.removeAttribute('hidden');
 }

 /* there has got to be a nicer way to implement disabled and hidden */
 #attributeChangedCallback(name, old, cur) {
  if (old != cur) {
   switch (name) {

    case "disabled": {
     const sheet = this.shadowRoot.styleSheets[1];
     if (this.disabled) {
      //sheet.insertRule(":host {opacity: .7;}", 0); // maybe use later
      this.#click_handler.handleEvent = () => {};
     }
     else  {
      if (sheet.cssRules.length) sheet.deleteRule(0);
      this.#click_handler.handleEvent = this.#onClick.bind(this);
     }
     break;
    }

    case "hidden": {
     const sheet = this.shadowRoot.styleSheets[2];
     if (this.hidden) {
      sheet.insertRule(":host {display: none;}", 0);
     }
     else  {
      if (sheet.cssRules.length) sheet.deleteRule(0);
     }
     break;
    }

   }
  }
 }


 nextPlayer() {
  if (this.#player == 0) this.#player = 1;
  else this.#player = 0;
 }

 #colorWin(matches) {
  const tiles = this.shadowRoot.querySelectorAll(".tile");
  for (let i = 0; i < tiles.length; i++) {
   if (!matches.includes(i)) tiles[i].classList.add("extraneous");
  }
 }

 horizontal(index) {
  const matches = [];
  const start = index - index % 3;
  for (let i = start; i < start + 3; i++) {
   if (this.#board[i] == this.#player) matches.push(i);
  }
  return matches;
 }

 vertical(index) {
  const matches = [];
  const start = index % 3;
  for (let i = start; i < 9; i += 3) {
   if (this.#board[i] == this.#player) matches.push(i);
  }
  return matches;
 }

 // top-left to down-right win
 tldr() {
  const matches = [];
  const start = 0;
  for (let i = start; i < 9; i += 4) {
   if (this.#board[i] == this.#player) matches.push(i);
  }
  return matches;
 }

 // top-right to down-left win
 trdl() {
  const matches = [];
  const start = 2;
  for (let i = start; i < 8; i += 2) {
   if (this.#board[i] == this.#player) matches.push(i);
  }
  return matches;
 }

 get criteria() {
  return [
   this.horizontal.bind(this),
   this.vertical.bind(this),
   this.tldr.bind(this),
   this.trdl.bind(this)
  ]
 }

 #onClick(event) {
  // grab the object that was clicked
  const target = event.explicitOriginalTarget;
  // check if the object is a tile
  if (target.classList.contains("tile")) {
   // check if that space in board memory is empty
   const index = parseInt(target.dataset.index);
   if (this.#board[index] == null) {
    // add tile if needed (make sure to add player class to tile)
    target.classList.add("p" + this.#player);
    this.#board[index] = this.#player;
    target.appendChild(this.getMark());
    // check to see if this was a winning play
    const longest_match = this.criteria.reduce((acc, criterion_func) => {
     const matches = criterion_func(index)
     if (matches.length > acc.length) return matches;
     else return acc;
    }, []);
    if (longest_match.length >= 3) {
     // disable board and color losing tiles
     this.#colorWin(longest_match);
     this.disabled = true;
     // add observable data-winner attribute
     this.setAttribute("data-winner", this.#player);
    }
    // check for catz game
    else if (!this.#board.includes(null)) {
     // disable board and color losing tiles
     this.#colorWin([]);
     this.disabled = true;
     // add observable data-winner attribute
     this.setAttribute("data-winner", -1);
    }
    // else next player
    else {
     this.nextPlayer();
    }
   }
  }
 }

 #getClickHandler() {
  const handler = {}
  handler.handleEvent = this.#onClick.bind(this)
  return handler;
 }

 #setStylesheets(root) {
  const style = document.createElement("style");
  style.textContent = TicTacToe.#default_styling;
  root.appendChild(style);
  root.appendChild(document.createElement("style")); // disabled
  root.appendChild(document.createElement("style")); // hidden
 }

 getMark() {
  return this.#marks[this.#player].cloneNode(true);
 }

 getState() {
  return Array.from(this.#board);
 }

 #getBars() {
  const bars = document.createElement("ul");
  bars.className = "bars";
  for (let i = 0; i < 4; i++) {
   const bar = document.createElement("div");
   bar.className = "bar";
   bars.appendChild(bar);
  }
  return bars;
 }

 #getTiles() {
  const tiles = document.createElement("ol");
  const tile = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  tile.setAttribute("width", "100");
  tile.setAttribute("height", "100");
  tile.setAttribute("viewBox", "0 0 100 100");
  tile.classList.add("tile");
  tiles.className = "tiles";
  for (let i = 0; i < 9; i++) {
   tile.setAttribute("data-index", i);
   tiles.appendChild(tile.cloneNode(true));
  }
  return tiles;
 }

}

export default TicTacToe;
